export const mockEnableBodyScroll: jest.Mock = jest.fn();
export const mockDisableBodyScroll: jest.Mock = jest.fn();

jest.mock('body-scroll-lock', (): {} => ({
	enableBodyScroll: mockEnableBodyScroll,
	disableBodyScroll: mockDisableBodyScroll
}));
