import {shallow, ShallowWrapper} from 'enzyme';
import React from 'react';
import {ProjectsSection} from '../../src/sections/ProjectsSection';
import {projects} from '../../src/DataProviders';

describe('ProjectsSection', (): void => {
	it('renders', (): void => {
		//when
		const wrapper: ShallowWrapper = shallow(<ProjectsSection projects={projects}/>);

		//then
		expect(wrapper).toMatchSnapshot();
	});

	it('renders without projects', (): void => {
		//when
		const wrapper: ShallowWrapper = shallow(<ProjectsSection projects={[]}/>);

		//then
		expect(wrapper).toMatchSnapshot();
	});
});
