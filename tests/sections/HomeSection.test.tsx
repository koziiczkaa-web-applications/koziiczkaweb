import '../__mocks__/MockI18n';
import {shallow, ShallowWrapper} from 'enzyme';
import React from 'react';
import {HomeSection} from '../../src/sections/HomeSection';

describe('HomeSection', (): void => {
	it('renders', (): void => {
		//when
		const wrapper: ShallowWrapper = shallow(<HomeSection/>);

		//then
		expect(wrapper).toMatchSnapshot();
	});
});
