import '../__mocks__/MockBodyScrollLock';
import {shallow, ShallowWrapper} from 'enzyme';
import React from 'react';
import {Header} from '../../src/components/Header';
import {POLISH} from '../../src/entities/Language';
import {mockDisableBodyScroll, mockEnableBodyScroll} from '../__mocks__/MockBodyScrollLock';

describe('Header', (): void => {
	it('renders', (): void  => {
		//when
		const wrapper: ShallowWrapper = shallow(<Header language={POLISH} onChange={(): void => undefined}/>);

		//then
		expect(wrapper).toMatchSnapshot();
	});

	it('opens mobile nav', (): void  => {
		//given
		const wrapper: ShallowWrapper = shallow(<Header language={POLISH} onChange={(): void => undefined}/>);

		//when
		wrapper.find('#hamburger-button').simulate('click');

		//then
		expect(wrapper).toMatchSnapshot();
		expect(mockEnableBodyScroll).toHaveBeenCalledTimes(0);
		expect(mockDisableBodyScroll).toHaveBeenCalledTimes(1);
		expect(mockDisableBodyScroll).toHaveBeenCalledWith(document.body);
	});

	it('closes mobile nav', (): void  => {
		//given
		const wrapper: ShallowWrapper = shallow(<Header language={POLISH} onChange={(): void => undefined}/>);

		//when
		wrapper.find('#hamburger-button').simulate('click');
		wrapper.find('#hamburger-button').simulate('click');

		//then
		expect(wrapper).toMatchSnapshot();
		expect(mockEnableBodyScroll).toHaveBeenCalledTimes(1);
		expect(mockEnableBodyScroll).toHaveBeenCalledWith(document.body);
		expect(mockDisableBodyScroll).toHaveBeenCalledTimes(1);
		expect(mockDisableBodyScroll).toHaveBeenCalledWith(document.body);
	});
});
