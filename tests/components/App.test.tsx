import {shallow, ShallowWrapper} from 'enzyme';
import React from 'react';
import App from '../../src/components/App';

describe('App', (): void => {
	it('renders', (): void => {
		//when
		const wrapper: ShallowWrapper = shallow(<App/>);

		//then
		expect(wrapper).toMatchSnapshot();
	});
});
