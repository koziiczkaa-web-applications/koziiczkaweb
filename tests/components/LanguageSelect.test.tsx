import {shallow, ShallowWrapper} from 'enzyme';
import React from 'react';
import {LanguageSelect} from '../../src/components/LanguagesSelect';
import {ENGLISH, POLISH, SPANISH} from '../../src/entities/Language';

describe('LanguageSelect', (): void => {
	it('renders', (): void => {
		//when
		const handleChange: jest.Mock = jest.fn();
		const wrapper: ShallowWrapper = shallow(<LanguageSelect
			elements={[]}
			language={POLISH}
			onChange={handleChange}
		/>);

		//then
		expect(handleChange).toHaveBeenCalledTimes(0);
		expect(wrapper).toMatchSnapshot();
	});

	it('renders with elements', (): void => {
		//when
		const wrapper: ShallowWrapper = shallow(<LanguageSelect
			elements={[POLISH, ENGLISH, SPANISH]}
			language={POLISH}
			onChange={(): void => undefined}
		/>);

		//then
		expect(wrapper).toMatchSnapshot();
	});

	it('handles click', (): void => {
		// given
		const handleChange: jest.Mock = jest.fn();
		const wrapper: ShallowWrapper = shallow(<LanguageSelect
			elements={[POLISH, ENGLISH, SPANISH]}
			language={POLISH}
			id='language-mobile'
			onChange={handleChange}
		/>);

		//when
		wrapper.find('#language-mobile').simulate('click');

		//then
		expect(handleChange).toHaveBeenCalledTimes(0);
		expect(wrapper).toMatchSnapshot();
	});

	it('handles double click', (): void => {
		// given
		const handleChange: jest.Mock = jest.fn();
		const wrapper: ShallowWrapper = shallow(<LanguageSelect
			elements={[POLISH, ENGLISH, SPANISH]}
			language={POLISH}
			id='language-mobile'
			onChange={handleChange}
		/>);

		//when
		wrapper.find('#language-mobile').simulate('click');
		wrapper.find('#language-mobile').simulate('click');

		//then
		expect(handleChange).toHaveBeenCalledTimes(0);
		expect(wrapper).toMatchSnapshot();
	});

	it('handles language change', (): void => {
		//given
		const handleChange: jest.Mock = jest.fn();
		const wrapper: ShallowWrapper = shallow(<LanguageSelect
			elements={[POLISH, ENGLISH, SPANISH]}
			language={POLISH}
			id='language-mobile'
			onChange={handleChange}
		/>);

		//when
		wrapper.find('#language-list_element-2').simulate('click');

		//then
		expect(wrapper).toMatchSnapshot();
		expect(handleChange).toHaveBeenCalledTimes(1);
		expect(handleChange).toHaveBeenCalledWith(SPANISH);
	});
});
