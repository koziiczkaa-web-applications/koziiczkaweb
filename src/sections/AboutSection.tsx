import React from 'react';
import {SectionTitle} from '../components/SectionTitle';
import './AboutSection.scss';
import {Button} from '../components/Button';
import {TechnologyTile} from '../components/TechnologyTile';
import ReactIcon from '../../resources/images/reactjs.svg';
import JSIcon from '../../resources/images/js.svg';
import SASS from '../../resources/images/sass.svg';
import Node from '../../resources/images/node.svg';
import Yarn from '../../resources/images/yarn.svg';
import TS from '../../resources/images/ts.svg';
import {Timeline} from '../components/Timeline';
import {Trans, useTranslation} from 'react-i18next';
import {timelineEducationElements, timelineWorkElements} from '../DataProviders';

export function AboutSection(): JSX.Element {
	const {t} = useTranslation();

	return <div className='about'>
		<SectionTitle title={t('about-section-title')}/>
		<div className='about-wrapper about-wrapper-upper'>
			<div className='about-col about-col-left'>
				<img className='about-img' src='../../resources/images/Me.jpg' alt='me'/>
			</div>
			<div className='about-col about-col-mid'>
				<h4 className='about-h4'>
					<Trans i18nKey='about-section-intro'/>
				</h4>
				<p className='about-paragraph'>
					<Trans i18nKey='about-section-desc'/>
				</p>
				<Button name={t('button-resume')}/>
			</div>
			<div className='about-col about-col-right'>
				<h4 className='about-h4'>
					<Trans i18nKey='about-section-technologies'/>
				</h4>
				<div className='about-gallery'>
					<TechnologyTile image={<ReactIcon/>} name='React'/>
					<TechnologyTile name='JavaScript (ES6+)' image={<JSIcon/>}/>
					<TechnologyTile name='Sass' image={<SASS/>}/>
					<TechnologyTile name='Node.js' image={<Node/>}/>
					<TechnologyTile name='Yarn' image={<Yarn/>}/>
					<TechnologyTile name='TypeScript' image={<TS/>}/>
				</div>
			</div>
		</div>
		<div className='about-wrapper about-wrapper-bottom'>
			<Timeline
				title={t('about-timeline-work')}
				elements={timelineWorkElements}
				renderName
			/>
			<Timeline
				title={t('about-timeline-education')}
				elements={timelineEducationElements}
				marginTop='3em'
			/>
		</div>
	</div>;
}
