import React from 'react';
import './ProjectsSection.scss';
import {SectionTitle} from '../components/SectionTitle';
import {SingleProject} from '../components/SingleProject';
import {Project} from '../entities/Project';
import {useTranslation} from 'react-i18next';

export interface ProjectsSectionProps {
	projects: Project[];
}

export function ProjectsSection(props: ProjectsSectionProps): JSX.Element {
	const {t} = useTranslation();

	return <div className='projects-wrapper'>
		<SectionTitle title={t('project-section-title')}/>
		{props.projects.map((p: Project, index: number): JSX.Element =>
			<SingleProject project={p} key={index}/>
		)}
	</div>;
}
