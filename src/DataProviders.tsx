import {Project} from './entities/Project';
import {faExternalLinkAlt} from '@fortawesome/free-solid-svg-icons';
import {faGitlab} from '@fortawesome/free-brands-svg-icons';
import {TimelineElement} from './entities/TimelineElement';

export const projects: Project[] = [
	{
		id: 1,
		title: 'KozikToolsApp',
		description: 'project-koziktools-desc',
		links: [
			{
				name: 'GitLab',
				icon: faGitlab
			},
			{
				name: 'View site',
				icon: faExternalLinkAlt
			}
		],
		technologies: ['React', 'SCSS', 'Jest']
	},
	{
		id: 2,
		title: 'KozikToolsApp',
		description: 'project-koziktools-desc',
		links: [
			{
				name: 'GitLab',
				icon: faGitlab
			},
			{
				name: 'View site',
				icon: faExternalLinkAlt
			}
		],
		technologies: ['React', 'SCSS', 'Jest']
	},
	{
		id: 3,
		title: 'KozikToolsApp',
		description: 'project-koziktools-desc',
		links: [
			{
				name: 'GitLab',
				icon: faGitlab
			},
			{
				name: 'View site',
				icon: faExternalLinkAlt
			}
		],
		technologies: ['React', 'SCSS', 'Jest']
	}
];

export const timelineWorkElements: TimelineElement[] = [
	{
		nameKey: 'Elte-s',
		positionKey: 'Frontend Developer',
		periodKey: 'about-timeline-work-eltes-period',
		descriptionKey: 'about-timeline-work-eltes-desc'
	},
	{
		nameKey: 'Komputronik',
		positionKey: 'about-timeline-work-komputronik-position',
		periodKey: 'about-timeline-work-komputronik-period',
		descriptionKey: 'about-timeline-work-komputronik-desc'
	}
];

export const timelineEducationElements: TimelineElement[] = [
	{
		nameKey: 'about-timeline-education-agh',
		positionKey: 'about-timeline-education-agh-title',
		periodKey: 'about-timeline-education-agh-period',
		descriptionKey: 'about-timeline-education-agh-desc'
	},
	{
		nameKey: 'about-timeline-education-technical-school',
		positionKey: 'about-timeline-education-technical-school-title',
		periodKey: 'about-timeline-education-technical-school-period',
		descriptionKey: 'about-timeline-education-technical-school-desc'
	}
];
