import React from 'react';
import './LanguageSelect.scss';
import {Language} from '../entities/Language';

export interface LanguagesSelectProps {
	elements: Language[];
	onChange: (language: Language) => void;
	language: Language;
	id?: string;
}

export function LanguageSelect(props: LanguagesSelectProps): JSX.Element {
	const [open, setOpen] = React.useState<boolean>(false);

	function handleClick(): void {
		setOpen(!open);
	}

	function handleChangeLanguage(index: number): void {
		props.onChange(props.elements[index]);
	}

	return <div className='language' id={props.id} onClick={handleClick}>
		<div className='language--chosen'>
			{props.language.icon}
			<span className='language-name'>{props.language.name}</span>
		</div>
		<ul className={'language-list' + (open ? ' language-list--opened' : '')}>
			{props.elements.map((element: Language, index: number): JSX.Element =>
				<li
					id={`language-list_element-${index}`}
					key={index}
					className={`language-list_element ${element === props.language ? 'language-list_element--active' : ''}`}
					onClick={(): void => handleChangeLanguage(index)}
				>
					{element.icon} {element.name}
				</li>
			)}
		</ul>
	</div>;
}
