import React from 'react';
import './SocialMediaTile.scss';

export interface SocialMediaTileProps {
	link: string;
	icon: JSX.Element;
}

export function SocialMediaTile(props: SocialMediaTileProps): JSX.Element {
	return <div className='socialmedia-item'>
		{props.icon}
		<span className='socialmedia-item_name'>{props.link}</span>
	</div>;
}
