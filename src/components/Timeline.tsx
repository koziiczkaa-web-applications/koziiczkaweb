import React from 'react';
import './Timeline.scss';
import {TimelineElement} from '../entities/TimelineElement';
import {useTranslation} from 'react-i18next';

export interface TimelineProps {
	title: string;
	elements: TimelineElement[];
	marginTop?: string;
	renderName?: boolean;
	id?: string;
}

export function Timeline(props: TimelineProps): JSX.Element {
	const [active, setActive] = React.useState<number>(0);

	const {t} = useTranslation();

	const activeElement: TimelineElement = props.elements[active];

	function renderElement(element: TimelineElement, index: number): JSX.Element {
		return <li
			title={t(element.nameKey)}
			key={index}
			id={`timeline-line_element-${index}`}
			className={`timeline-line_element ${index === active ? 'timeline-line_element--active' : ''}`}
			onClick={(): void => setActive(index)}
		>
			{t(element.nameKey)}
		</li>;
	}

	function renderElementInfo(): JSX.Element {
		return activeElement && <div className='timeline-info'>
			<h4 className='timeline-info_title'>{t(activeElement.positionKey)}
				{props.renderName && <>
					{t('about-timeline-work-conjunction')}
					<span className='timeline-info_title--name'>{t(activeElement.nameKey)}</span>
				</>}
			</h4>
			<p className='timeline-info__period'>{t(activeElement.periodKey)}</p>
			<ul className='timeline-info__list'>
				{t(activeElement.descriptionKey).split('\n').map((desc: string, index: number): JSX.Element =>
					<li key={index} className='timeline-info__list_element'>{desc}</li>
				)}
			</ul>
		</div>;
	}

	return <div className='timeline'>
		<h3 className='timeline-title' style={{marginTop: props.marginTop}}>{props.title}</h3>
		<div className='timeline-wrapper'>
			<div className='timeline-line_wrapper'>
				<ul className='timeline-line'>
					{props.elements.map(renderElement)}
				</ul>
			</div>
			{renderElementInfo()}
		</div>
	</div>;
}
