import * as React from 'react';
import {Header} from './Header';
import {HomeSection} from '../sections/HomeSection';
import './App.scss';
import {AboutSection} from '../sections/AboutSection';
import i18next from 'i18next';
import {initReactI18next} from 'react-i18next';
import en from '../translations/en.json';
import pl from '../translations/pl.json';
import es from '../translations/es.json';
import {ENGLISH, Language} from '../entities/Language';
import {ProjectsSection} from '../sections/ProjectsSection';
import {projects} from '../DataProviders';
import {FooterSection} from '../sections/FooterSection';

export function App(): JSX.Element {
	const [translationLoad, setTranslationLoad] = React.useState<boolean>(false);
	const [language, setLanguage] = React.useState<Language>(ENGLISH);
	const [counter, setCounter] = React.useState<number>(0);

	React.useEffect((): void => {
		i18next
			.use(initReactI18next)
			.init({
				lng: 'en',
				fallbackLng: 'en',
				resources: {en, pl, es}
			})
			.then((): void => setTranslationLoad(true));
	}, []);

	React.useEffect((): void => {
		i18next.changeLanguage(language.code || 'en')
			.then((): void => setCounter(counter + 1));
	}, [language]);

	function handleLanguageChange(newLanguage: Language): void {
		setLanguage(newLanguage);
	}

	if (!translationLoad) {
		return <></>;
	}

	return <>
		<Header onChange={handleLanguageChange} language={language}/>
		<div className='main'>
			<HomeSection/>
			<AboutSection/>
			<ProjectsSection projects={projects}/>
			<FooterSection/>
		</div>
	</>;
}

export default App;
