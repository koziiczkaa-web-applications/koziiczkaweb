import React from 'react';
import './Header.scss';
import {HamburgerButton} from './HamburgerButton';
import {disableBodyScroll, enableBodyScroll} from 'body-scroll-lock';
import {NavBarMobile} from './NavBarMobile';
import {Trans} from 'react-i18next';
import {Language} from '../entities/Language';

export interface HeaderProps {
	onChange: (language: Language) => void;
	language: Language;
}

export function Header(props: HeaderProps): JSX.Element {
	const [open, setOpen] = React.useState<boolean>(false);

	function handleClick(): void {
		setOpen(!open);
		if (open) {
			enableBodyScroll(document.body);
		} else {
			disableBodyScroll(document.body);
		}
	}

	return <header className='header'>
		<ul className='header-wrapper'>
			<li className='header-item header-item-logo'>
				Alicja Kozik
			</li>
			<li className='header-item header-item--hamburger'>
				<HamburgerButton
					id='hamburger-button'
					onClick={handleClick}
					opened={open}
				/>
			</li>
			<li className='header-item header-item--desktop'>
				<a href='#'><Trans i18nKey='navigation-home'/></a>
			</li>
			<li className='header-item header-item--desktop'>
				<a href='#'><Trans i18nKey='navigation-about-me'/></a>
			</li>
			<li className='header-item header-item--desktop'>
				<a href='#'><Trans i18nKey='navigation-projects'/></a>
			</li>
			<li className='header-item header-item--desktop'>
				<a href='#'><Trans i18nKey='navigation-contact'/></a>
			</li>
		</ul>
		<NavBarMobile
			opened={open}
			onChange={props.onChange}
			language={props.language}
		/>
	</header>;
}
