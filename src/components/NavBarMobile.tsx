import React from 'react';
import './NavBarMobile.scss';
import {LanguageSelect} from './LanguagesSelect';
import {Trans} from 'react-i18next';
import {ENGLISH, Language, POLISH, SPANISH} from '../entities/Language';

export interface NavBarMobileProps {
	opened: boolean;
	onChange: (language: Language) => void;
	language: Language;
}

export function NavBarMobile(props: NavBarMobileProps): JSX.Element {
	return <div className={'navbar-mobile' + (props.opened ? ' navbar-mobile--active' : '')}>
		<LanguageSelect
			elements={[POLISH, ENGLISH, SPANISH]}
			onChange={props.onChange}
			language={props.language}
			id='language-mobile'
		/>

		<ul className='navbar-mobile__list'>
			<li className='navbar-mobile__item'>
				<a href='#' className='navbar-mobile__link'>
					<Trans i18nKey='navigation-home'/>
				</a>
			</li>
			<li className='navbar-mobile__item'>
				<a href='#' className='navbar-mobile__link'>
					<Trans i18nKey='navigation-about-me'/>
				</a>
			</li>
			<li className='navbar-mobile__item'>
				<a href='#' className='navbar-mobile__link'>
					<Trans i18nKey='navigation-projects'/>
				</a>
			</li>
			<li className='navbar-mobile__item'>
				<a href='#' className='navbar-mobile__link'>
					<Trans i18nKey='navigation-contact'/>
				</a>
			</li>
		</ul>
	</div>;
}
