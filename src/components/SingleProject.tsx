import React from 'react';
import './SingleProject.scss';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {Link} from '../entities/Link';
import styled, {DefaultTheme, StyledComponent} from 'styled-components';
import {Project} from '../entities/Project';
import {useTranslation} from 'react-i18next';

export interface SingleProjectProps {
	project: Project;
}

export function SingleProject(props: SingleProjectProps): JSX.Element {
	const {t} = useTranslation();

	const ProjectTitle: StyledComponent<'h3', DefaultTheme> = styled.h3`
      &:before {
        content: '0${props.project.id}.';
        margin-right: .5em;
        font-size: 16px;
        color: var(--clr-pink);
      }
	`;

	return <div className='project'>
		<ProjectTitle>{props.project.title}</ProjectTitle>
		<div className='project-image'/>
		<div className='project-info-wrapper'>
			<div className='project-info'>
				<p className='project-info-desc'>{t(props.project.description)}</p>
				<ul className='project-info-list'>
					{props.project.technologies.map((technology: string, index: number): JSX.Element =>
						<li className='project-info-element' key={index}>{technology}</li>
					)}
				</ul>
			</div>
			<ul className='project-links'>
				{props.project.links.map((link: Link, index: number): JSX.Element =>
					<a href='#' key={index}>
						<li className='project-links-element'>
							<FontAwesomeIcon icon={link.icon} className='project-links-icon'/>
							{link.name}
						</li>
					</a>
				)}
			</ul>
		</div>
	</div>;
}
