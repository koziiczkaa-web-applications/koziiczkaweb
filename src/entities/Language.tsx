import Polish from '../../resources/images/211-poland.svg';
import English from '../../resources/images/260-united-kingdom.svg';
import Spanish from '../../resources/images/128-spain.svg';
import React from 'react';

export interface Language {
	readonly name: string;
	readonly icon: JSX.Element;
	readonly code: string;
}

export const POLISH: Language = {
	name: 'Polish',
	icon: <Polish/>,
	code: 'pl'
};

export const ENGLISH: Language = {
	name: 'English',
	icon: <English/>,
	code: 'en'
};

export const SPANISH: Language = {
	name: 'Spanish',
	icon: <Spanish/>,
	code: 'es'
};
