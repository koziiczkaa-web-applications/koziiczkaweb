import {IconProp} from '@fortawesome/fontawesome-svg-core';

export interface Link {
	readonly name: string;
	readonly icon: IconProp;
}
