export interface TimelineElement {
	readonly nameKey: string;
	readonly positionKey: string;
	readonly descriptionKey: string;
	readonly periodKey: string;
}
